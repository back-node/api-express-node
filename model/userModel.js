const mongoClient = require("mongodb").MongoClient;
const mongodbConfig = require("./../config").mongodb;
const ObjectId = require("mongodb").ObjectId;

module.exports.getUser = cb => {
  mongoClient.connect(mongodbConfig.mongodb_url, (err, client) => {
    if (err) throw err;
    client.db("upselly").collection("clients").find().toArray((err, data) => {
      client.close();
      cb(data);
    });
  });
};
module.exports.getUserPromise = () => {
  return new Promise((resolve, reject) => {
    return mongoClient.connect(mongodbConfig.mongodb_url, (err, client) => {
      if (err) return reject(err);
      return client
        .db("upselly")
        .collection("clients")
        .find()
        .toArray((err, data) => {
          client.close();
          if (err) return reject(err);
          return resolve(data);
        });
    });
  });
};
module.exports.getUserById = id => {
  return new Promise((resolve, reject) => {
    return mongoClient.connect(mongodbConfig.mongodb_url, (err, client) => {
      if (err) return reject(err);
      return client
        .db("upselly")
        .collection("clients")
        .find({ _id: ObjectId(id) })
        .toArray((err, data) => {
          client.close();
          if (err) return reject(err);
          return resolve(data);
        });
    });
  });
};
module.exports.deleteUserById = id => {
  return new Promise((resolve, reject) => {
    return mongoClient.connect(mongodbConfig.mongodb_url, (err, client) => {
      if (err) return reject(err);
      client
        .db("upselly")
        .collection("clients")
        .remove({ _id: ObjectId(id) }, (err, res) => {
          if (err) reject(err);
          else {
            client.close();
            return resolve({ _id: id });
          }
        });
    });
  });
};
module.exports.addUserById = element => {
  return new Promise((resolve, reject) => {
    return mongoClient.connect(mongodbConfig.mongodb_url, (err, client) => {
      if (err) return reject(err);
      client
        .db("upselly")
        .collection("clients")
        .insertOne(element, (err, res) => {
          if (err) reject(err);
          else {
            client.close();
            return resolve(res.ops);
          }
        });
    });
  });
};
module.exports.updateById = (element, user) => {
  return new Promise((resolve, reject) => {
    return mongoClient.connect(mongodbConfig.mongodb_url, (err, client) => {
      if (err) return reject(err);
      client
        .db("upselly")
        .collection("clients")
        .update({ _id: ObjectId(user) }, element, (err, res) => {
          if (err) reject(err);
          else {
            client.close();
            //return resolve({ ...element, _id: ObjectId(user) });
            return resolve(res.ops[0]);
          }
        });
    });
  });
};
