const http = require("http");
const app = require("./../app.js");
const serverConfig = require("../config").server;
const server = http.createServer(app);
const port = serverConfig.port;
server.listen(port, err => {
  if (err) {
    console.log(err);
  } else {
    console.log(port);
  }
});
