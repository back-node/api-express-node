const express = require("express");
let router = express.Router();
const nodemailer = require("nodemailer");

const userModel = require("./../model/userModel");

router.get("/user", (req, res) => {
  userModel.getUser(data => {
    res.send(data);
  });
});

router.get("/userPromise", (req, res) => {
  //console.log();
  userModel
    .getUserPromise()
    .then(data => res.send(data))
    .catch(err => res.send(err));

  //res.end(typeof userModel.getUserPromise());
});
router.get("/userById/:user", (req, res) => {
  //console.log();
  const userId = req.params.user;
  userModel
    .getUserById(userId)
    .then(data => res.send(data))
    .catch(err => res.send(err));

  //res.end(typeof userModel.getUserPromise());
});
router.delete("/deleteUserById/:user", (req, res) => {
  //console.log();
  const userId = req.params.user;
  userModel
    .deleteUserById(userId)
    .then(data => res.send(data))
    .catch(err => res.send(err));

  //res.end(typeof userModel.getUserPromise());
});
router.post("/addOneUser", (req, res) => {
  //console.log();

  const newElement = {
    lastname: req.body.lastname,
    nickname: req.body.nickname,
  };
  userModel
    .addUserById(newElement)
    .then(data => res.send(data))
    .catch(err => res.send(err));

  //res.end(typeof userModel.getUserPromise());
});
router.put("/updateById/:user", (req, res) => {
  //console.log();
  const userId = req.params.user;
  const newElement = {
    lastname: req.body.lastname,
    nickname: req.body.nickname,
  };
  userModel
    .updateById(newElement, userId)
    .then(data => res.send(data))
    .catch(err => res.send(err));

  //res.end(typeof userModel.getUserPromise());
});
router.post("/sendMail", (req, res) => {
  nodemailer.createTestAccount((err, account) => {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      service: "Gmail",
      //port: 587,
      //secure: false, // true for 465, false for other ports
      auth: {
        user: "freddy.jopha@upsell.fr", // generated ethereal user
        pass: "fJop2017", // generated ethereal password
      },
    });

    // setup email data with unicode symbols
    let mailOptions = {
      from: '"Fred Foo 👻" <foo@example.com>', // sender address
      to: "freddy.jopha@upsell.fr", // list of receivers
      subject: "Hello ✔", // Subject line
      text: "Hello world?", // plain text body
      html: "<b>Hello world?</b>", // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      //console.log("Message sent: %s", info.messageId);
      res.send(info.messageId);
      // Preview only available when sending through an Ethereal account
      //console.log("Preview URL: %s", );

      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
      // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    });
  });
});

module.exports = router;
